<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\jabatan\JabatanController;
use App\Http\Controllers\karyawan\karyawanController;
use App\Http\Controllers\transaksi\TransaksiController;
use App\Http\Controllers\gaji\GajiController;
use App\Http\Controllers\user\UserController;
use App\Http\Controllers\auth\AuthController;
use App\Http\Controllers\dashboard\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function () {
    return view('dashboard.login.login');
});
Route::get('utama', [DashboardController::class, 'dashboard'])->name('dasboard');

Route::post('login', [AuthController::class, 'login'])->name('login.action');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::Resource('/gaji', GajiController::class);
Route::Resource('/master/jabatan', JabatanController::class);
Route::Resource('/master/karyawan', karyawanController::class);
Route::Resource('/transaksi', TransaksiController::class);

Route::put('/account-active/{id}', [UserController::class, 'activate'])->name('account-active.activate');
Route::put('/account-ban/{id}', [UserController::class, 'ban'])->name('account-ban.ban');
Route::Resource('/account', UserController::class);

Route::get('/kabupaten', [karyawanController::class, 'kabupaten'])->name('kabupaten.kabupaten');
Route::get('/kecamatan', [karyawanController::class, 'kecamatan'])->name('kecamatan.kecamatan');