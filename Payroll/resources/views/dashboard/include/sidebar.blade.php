<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="">
            <span class="align-middle">
                <img src="{{asset('assets/1648607872_favicon.png')}}" alt="Admin Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">PayRoll</span>
            </span>
        </a>
        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Dashboard
            </li>

            <li class="sidebar-item {{(request()->is('dashboard')) ? 'active' : ''}}">
                <a class="sidebar-link" href="">
                    <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                </a>
            </li>

            <li class="sidebar-header ">
                Adminisistrasi
            </li>


            <li class="sidebar-item {{(request()->is('gaji')) ? 'active' : ''}}">
                <a class="sidebar-link" href="{{route('gaji.index')}}">
                    <i class="align-middle" data-feather="dollar-sign"></i> <span class="align-middle">Gaji</span>
                </a>
            </li>
            @if (Auth::user()->id_role==1)
            <li class="sidebar-item {{(request()->is('transaksi')) ? 'active' : ''}}">
                <a class="sidebar-link" href="{{route('transaksi.index')}}">
                    <i class="align-middle" data-feather="award"></i> <span class="align-middle">Bonus</span>
                </a>
            </li>
            @endif
            <li class="sidebar-header">
                Setting
            </li>
            <li class="sidebar-item {{(request()->is('master*')) ? 'active' : ''}}">
                <a class="sidebar-link collapsed" data-bs-target="#data-master" data-bs-toggle="collapse">
                    <i class="align-middle" data-feather="database"></i> <span class="align-middle">Data Master</span>
                </a>
                @if (Auth::user()->id_role==1)
                    <ul class="sidebar-dropdown list-unstyled collapse" id="data-master" data-parent="#sidebar">
                        <li class="sidebar-item">
                            <a class="sidebar-link ms-3" href="{{route('jabatan.index')}}">
                                <i class="fas fa-long-arrow-alt-right"></i>
                                Jabatan
                            </a>
                        </li>
                    </ul> 
                @endif
                
                <ul class="sidebar-dropdown list-unstyled collapse" id="data-master" data-parent="#sidebar">
                    <li class="sidebar-item">
                        <a class="sidebar-link ms-3" href="{{route('karyawan.index')}}">
                            <i class="fas fa-long-arrow-alt-right"></i>
                           Karyawan
                        </a>
                    </li>
                </ul>
            </li>
            @if (Auth::user()->id_role==1)
            <li class="sidebar-item {{(request()->is('account')) ? 'active' : ''}}">
                <a class="sidebar-link" href="{{route('account.index')}}">
                    <i class="align-middle" data-feather="user"></i> <span class="align-middle">User</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>