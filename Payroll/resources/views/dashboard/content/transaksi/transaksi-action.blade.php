@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">
  <div class="row d-flex justify-content-center">
    <div class="col-8">
        @if($action == 'add')
            <h1 class="h3 mb-3">Add New Transaksi</h1>
        @else
            <h1 class="h3 mb-3">Edit Transaksi</h1>
        @endif
      <div class="card">
        <div class="card-header">
          <h5 class="card-title mb-0">
            @if($action == 'add')
                <form id="transaksi-action" action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
            @else
                <form id="transaksi-action" action="{{ route('transaksi.update', $transaksi->id) }}" method="post" enctype="multipart/form-data">
                @method('put')
            @endif
                @csrf
                <div class="mb-3">
                    <label for="fullname" class="form-label">Kode</label><span class="text-danger">*</span>
                    <input type="text" name="kode" class="form-control @error('kode') is-invalid @enderror" value="{{ old('kode') }}" id="kode" placeholder="kode anda">
                    @error('kode')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="fullname" class="form-label">nominal</label><span class="text-danger">*</span>
                    <input type="number" name="nominal" class="form-control @error('nominal') is-invalid @enderror" value="{{ old('nominal') }}" id="nominal" placeholder="nominal anda">
                    @error('nominal')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                  <div class="row targetDiv" id="div0">
                    <div class="col-md-12">
                      <div id="group1" class="fvrduplicate">
                        @if ($action=='edit')
                          @foreach ($transaksi->detail as $value)
                            <div class="row entry">
                              <div class="col-xs-12 col-md-5">
                                <div class="form-group">
                                  <label>Karyawan</label>
                                  <select class="form-select @error('id_karyawan') is-invalid @enderror" name="id_karyawan[]" aria-label="Default select example" id="id_karyawan[]">
                                    <option selected disabled value="">Karyawan Anda</option>
                                    @foreach ($karyawan as $item)
                                        <option value="{{$item['id']}}" <?php if($value->id_karyawan == $item['id']) { echo "selected";} ?>>{{$item['nama']}}</option>
                                    @endforeach
                                    @if(old('id_karyawan'))
                                        @foreach($karyawan as $item)
                                        <option value="{{$item['id']}}" {{ ($item['id'] == old('id_karyawan')) ? 'selected="selected"' : '' }}>{{$item['nama']}}</option>
                                        @endforeach
                                    @endif
                                  </select>
                                  @error('id_karyawan')
                                      <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                                </div>
                              </div>
                              <div class="col-xs-12 col-md-5">
                                <div class="form-group">
                                  <label>Persen</label>
                                  <input class="form-control form-control-sm" name="persen[]" type="number" value="{{$value->persen}}" placeholder="10">
                                </div>
                              </div>
                              <div class="col-xs-12 col-md-2">
                                <div class="form-group">
                                  <label>&nbsp;</label>
                                  <button type="button" class="btn btn-success btn-sm btn-add">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                        <div class="row entry">
                          <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                              <label>Karyawan</label>
                              <select class="form-select @error('id_karyawan') is-invalid @enderror" name="id_karyawan[]" aria-label="Default select example" id="id_karyawan[]">
                                <option selected disabled value="">Karyawan Anda</option>
                                @foreach ($karyawan as $item)
                                    <option value="{{$item['id']}}">{{$item['nama']}}</option>
                                @endforeach
                                @if(old('id_karyawan'))
                                    @foreach($karyawan as $item)
                                    <option value="{{$item['id']}}" {{ ($item['id'] == old('id_karyawan')) ? 'selected="selected"' : '' }}>{{$item['nama']}}</option>
                                    @endforeach
                                @endif
                              </select>
                              @error('id_jabatan')
                                  <div class="invalid-feedback">{{ $message }}</div>
                              @enderror
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                              <label>Persen</label>
                              <input class="form-control form-control-sm" name="persen[]" type="number" placeholder="10">
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group">
                              <label>&nbsp;</label>
                              <button type="button" class="btn btn-success btn-sm btn-add">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              
              

              <div class="d-flex justify-content-center">
                <a href="{{route('transaksi.index')}}" class="btn btn-light me-2">
                    <span class="btn-icon-label">
                      <i data-feather="arrow-left" class="me-2"></i>
                        <span> Kembali </span>
                    </span>
                </a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </h5>
        </div>
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('page-script')
<script>

@if($action=='edit')
  const transaksi = {!! json_encode($transaksi->toArray()) !!}

    kode.value = transaksi.kode
    nominal.value = transaksi.nominal

@endif

                
$(function() {
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();
        var controlForm = $(this).closest('.fvrduplicate'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);
        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<i class="fa fa-minus" aria-hidden="true"></i>');
    }).on('click', '.btn-remove', function(e) {
        $(this).closest('.entry').remove();
        return false;
    });
});
</script>
@endsection