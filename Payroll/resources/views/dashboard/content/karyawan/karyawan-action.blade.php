@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">
  <div class="row d-flex justify-content-center">
    <div class="col-8">
        @if($action == 'add')
            <h1 class="h3 mb-3">Add New Karyawan</h1>
        @else
            <h1 class="h3 mb-3">Edit Karyawan</h1>
        @endif
      <div class="card">
        <div class="card-header">
          <h5 class="card-title mb-0">
            @if($action == 'add')
                <form id="karyawan-action" action="{{ route('karyawan.store') }}" method="POST" enctype="multipart/form-data">
            @else
                <form id="karyawan-action" action="{{ route('karyawan.update', $karyawan->id) }}" method="post" enctype="multipart/form-data">
                @method('put')
            @endif
                @csrf
                <div class="mb-3">
                    <label for="fullname" class="form-label">Nama Lengkap</label><span class="text-danger">*</span>
                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}" id="nama" placeholder="Nama anda">
                    @error('nama')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                  <label for="fullname" class="form-label">Email</label><span class="text-danger">*</span>
                  <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" id="email" placeholder="email anda">
                  @error('email')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
                <!-- Jabatan -->
                <div class="mb-3">
                    <label for="job" class="form-label">Jabatan</label><span class="text-danger">*</span>
                    <select class="form-select @error('id_jabatan') is-invalid @enderror" name="id_jabatan" aria-label="Default select example" id="id_jabatan">
                    <option selected disabled value="">Jabatan Anda</option>
                    @foreach ($jabatan as $item)
                        <option value="{{$item['id']}}">{{$item['nama']}}</option>
                    @endforeach
                    @if ($action == 'edit')
                      @foreach ($jabatan as $item)
                          <option value="{{$item['id']}}" <?php if($karyawan->id_jabatan == $item['id']) { echo "selected";} ?>>{{$item['nama']}}</option>
                      @endforeach
                    @endif
                    @if(old('id_jabatan'))
                        @foreach($jabatan as $item)
                        <option value="{{$item['id']}}" {{ ($item['id'] == old('id_jabatan')) ? 'selected="selected"' : '' }}>{{$item['nama']}}</option>
                        @endforeach
                    @endif
                    </select>
                    @error('id_jabatan')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

              
              <!-- Provinsi -->
              <div class="mb-3">
                    <label for="category" class="form-label">Provinsi</label><span class="text-danger">*</span>
                    <select class="form-select @error('provinsi') is-invalid @enderror" name="provinsi" id="provinsi" >                
                    <option selected disabled value="">Provinsi</option>
                    @foreach($data_provinsi as $provinsi)
                        <option value="{{$provinsi['id']}}" >{{$provinsi['name']}}</option>
                    @endforeach

                    @if ($action == 'edit')
                      @foreach($data_kabupaten as $kabupaten)
                        @foreach($data_provinsi as $provinsi)
                          @if($karyawan->kabupaten==$kabupaten['id'])
                              <option value="{{$provinsi['id']}}" <?php if($kabupaten['province_id'] == $provinsi['id']) { echo "selected";} ?>>{{$provinsi['name']}}</option>
                          @endif
                        @endforeach
                      @endforeach
                    @endif
                    
                    @if(old('provinsi'))
                      @foreach($data_kabupaten as $kabupaten)
                       <option value="{{$provinsi['id']}}" {{ ($kabupaten['province_id'] == old('provinsi')) ? 'selected="selected"' : '' }}>{{$provinsi['name']}}</option>
                             
                      @endforeach
                    @endif
                    </select>
                    @error('provinsi')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

              <!-- Kab/Kota -->
              <div class="mb-3">
                <label for="category" class="form-label">Kab/Kota</label><span class="text-danger">*</span>
                <select class="form-select @error('kabupaten') is-invalid @enderror"  name="kabupaten" id="kabupaten" >                
                  <option selected disabled value="">Kab/Kota</option>

                  @if ($action == 'edit')
                    @foreach($data_kabupaten as $kabupaten)
                      <option value="{{$kabupaten['id']}}" <?php if($karyawan->kabupaten == $kabupaten['id']) { echo "selected";} ?>>{{$kabupaten['name']}}</option>
                    @endforeach  
                  @endif
                  
                  @if(old('kabupaten'))
                    @foreach($data_kabupaten as $kabupaten)
                      <option value="{{$kabupaten['id']}}" {{ ($kabupaten['id'] == old('kabupaten')) ? 'selected="selected"' : '' }}>{{$kabupaten['name']}}</option>
                    @endforeach  
                  @endif
                </select>
                @error('kabupaten')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              <!-- Kecamatan -->
              <div class="mb-3">
                <label for="category" class="form-label">Kecamatan</label><span class="text-danger">*</span>
                <select class="form-select @error('kecamatan') is-invalid @enderror" name="kecamatan" id="kecamatan" >
                  <option selected disabled value="">Kecamatan</option>

                  @if ($action == 'edit')
                    @foreach($data_kecamatan as $kecamatan)
                      <option value="{{$kecamatan['id']}}" <?php if($karyawan->kecamatan == $kecamatan['id']) { echo "selected";} ?>>{{$kecamatan['name']}}</option>
                    @endforeach 
                  @endif
                  
                  @if(old('kecamatan'))
                    @foreach($data_kecamatan as $kecamatan)
                      <option value="{{$kecamatan['id']}}" {{ ($kecamatan['id'] == old('kecamatan')) ? 'selected="selected"' : '' }}>{{$kecamatan['name']}}</option>
                    @endforeach  
                  @endif            
                </select>
                @error('kecamatan')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              <!-- Alamat -->
              <div class="mb-3">
                <label for="fullname" class="form-label">Alamat</label><span class="text-danger">*</span>
                <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="alamat" placeholder="Alamat anda" value="{{ old('alamat') }}">
                @error('alamat')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
              <!-- nomer rekening -->
              <div class="mb-3">
                <label for="contact" class="form-label">Rekening</label><span class="text-danger">*</span>
                <input type="text" name="rekening" class="form-control @error('rekening') is-invalid @enderror" id="rekening" placeholder="Rekening" value="{{ old('rekening') }}">
                @error('rekening')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              <div class="d-flex justify-content-center">
                <a href="{{route('karyawan.index')}}" class="btn btn-light me-2">
                    <span class="btn-icon-label">
                      <i data-feather="arrow-left" class="me-2"></i>
                        <span> Kembali </span>
                    </span>
                </a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </h5>
        </div>
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('page-script')
<script>

@if($action=='edit')
  const karyawan = {!! json_encode($karyawan->toArray()) !!}
    nama.value = karyawan.nama
    email.value = karyawan.email
    rekening.value = karyawan.rekening
    alamat.value = karyawan.alamat
@endif
  
    function onChangeSelect(url, id, name) {
        // send ajax request to get the cities of the selected province and append to the select tag
        $.ajax({
          url: url,
          type: 'GET',
          dataType: "json",
          data: {
            id: id
          },
          success: function (data) {
            
            $('#' + name).empty();
            if(name=="provinsi"){
              $('#' + name).append('<option selected disabled value="">Provinsi</option> ');
            }else if(name=="kabupaten"){
              $('#' + name).append('<option selected disabled value="">Kab/Kota</option> ');
            }else if(name=="kecamatan"){
              $('#' + name).append('<option selected disabled value="">Kecamatan</option> ');
            }
            data.forEach(el=>{
              $('#' + name).append('<option value="' + el.id + '">' + el.name    + '</option>');
            })
              // $.each(data, function (key, value) {
              //   console.log(key + value);
              //   // $('#sub_district' + name).append('<option value="' + key + '">' + value + '</option>');
              // });
          }
        });        
      }
      $(function () {
        $('#provinsi').on('change', function () {
          onChangeSelect('{{ route("kabupaten.kabupaten") }}', $(this).val(), 'kabupaten');
        });
        $('#kabupaten').on('change', function () {
          onChangeSelect('{{ route("kecamatan.kecamatan") }}', $(this).val(), 'kecamatan');
        });
      });

</script>
@endsection