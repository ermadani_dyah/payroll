@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">

  <h1 class="h3 mb-3">Karyawan</h1>

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pt-4">
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="card-title mb-0">Karyawan</h5>
            <a href="{{route('karyawan.create')}}" type="button" class="btn btn-primary">
              Add Karyawan</a>
          </div>

        </div>
        <div class="card-body">
          <table id="MyTable" class="table table-bordered" style="width:100%">
            <thead class="table-primary">
              <tr class="text-center">
                <th>
                  No.
                </th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Rekening</th>
                <th>Jabatan</th>
                @if (Auth::user()->id_role==1)
                  <th>Action</th>
                @endif
              </tr>
            </thead>
            <tbody>
              @forelse($karyawan as $item)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <span class="d-block mb-1">Provinsi : 
                        @foreach($data_kabupaten as $kabupaten)
                            @foreach($data_provinsi as $provinsi)
                                @if($item->kabupaten==$kabupaten['id'])
                                    @if ($kabupaten['province_id']==$provinsi['id'])
                                        <div class="col-9">{{$provinsi['name']}}</div>
                                    @endif
                                @endif
                            @endforeach
                        @endforeach</span>
                    <span class="d-block">Kab: 
                        @foreach ($data_kabupaten as $value)
                            @if ($item->kabupaten==$value['id'])
                            <div class="col-9">{{$value['name']}}</div>
                            @endif
                        @endforeach
                    </span>
                    <span class="d-block">Kec: 
                        @foreach ($data_kecamatan as $value)
                            @if ($item->kecamatan==$value['id'])
                                <div class="col-9">{{$value['name']}}</div>
                            @endif
                        @endforeach
                    </span>
                    <span class="d-block">Alamat: {{$item->alamat}}</span>
                    
                </td>
                <td>{{$item->rekening}}</td>
                <td>{{$item->jabatan->nama}}</td>
                @if (Auth::user()->id_role==1)
                  <td width="10" class="text-center">
                    <div class="d-flex justify-content-center">
                      <a class="btn btn-success" href="{{ route('karyawan.edit', $item->id) }}" role="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                          <i class="fas fa-edit"></i>
                      </a>
                      <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delete-modal" data-bs-id="{{$item->id}}" data-bs-act="{{ route('karyawan.destroy', $item->id) }}" data-bs-nama="{{$item->nama}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Delate">
                          <i class="align-middle" data-feather="trash-2"></i>
                      </button>
                    </div>
                  </td>
                @endif
                
              </tr>
              @empty
              <tr>
                <td colspan="8">
                  <center>Data kosong</center>
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-confirmation fade" id="delete-modal" tabindex='-1' role="dialog" aria-labelledby="deleteData" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
          <form id="delete-form" action="/" method="post" enctype="multipart/form-data">
              @csrf
              <div class="modal-body">
                  <h4>Konfirmasi</h4>
                  <p class="mb-0">Yakin untuk menghapus <strong id="del-name"> </strong></p>
              </div>
              <div class="modal-footer">
                  @method('DELETE')
                  <button type="button" class="btn btn-link" data-bs-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-danger">Hapus</button>
              </div>
          </form>
      </div>
  </div>
</div>

@endsection
<script>
  document.addEventListener("DOMContentLoaded", x => {

      const deleteModal = document.getElementById('delete-modal');
      const deleteForm = document.getElementById('delete-form');

      // Delete Modal Script
      deleteModal.addEventListener('show.bs.modal', evt => {
          let deleteBtn = event.relatedTarget;
          deleteForm.action = deleteBtn.getAttribute('data-bs-act');
          deleteModal.querySelector('#del-name').textContent = '"' + deleteBtn.getAttribute('data-bs-nama') + '" ?'
      })

  });
</script>