@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">
  <div class="row d-flex justify-content-center">
    <div class="col-8">
      @if($action == 'add')
        <h1 class="h3 mb-3">Add New User</h1>
      @else
        <h1 class="h3 mb-3">Edit User</h1>
      @endif
      <div class="card">
        <div class="card-header">
          <h5 class="card-title mb-0">
            @if($action == 'add')
              <form id="account-action" action="{{ route('account.store') }}" method="POST" enctype="multipart/form-data">
            @else
              <form id="account-action" action="{{ route('account.update', $account->id) }}" method="post" enctype="multipart/form-data">
              @method('put')
            @endif
            @csrf
            <div class="mb-3">
              <label for="fullname" class="form-label">Karyawan</label><span class="text-danger">*</span>
              <select class="form-select @error('id_karyawan') is-invalid @enderror" name="id_karyawan" aria-label="Default select example" id="id_karyawan">
                <option selected disabled value="">Karyawan Anda</option>
                  @foreach ($karyawan as $item)
                    <option value="{{$item['id']}}">{{$item['nama']}}</option>
                  @endforeach
                @if ($action=="edit")
                  @foreach ($karyawan as $item)
                    <option value="{{$item['id']}}" <?php if($account->id_karyawan == $item['id']) { echo "selected";} ?>>{{$item['nama']}}</option>
                  @endforeach
                @endif
                @if(old('id_karyawan'))
                    @foreach($karyawan as $item)
                    <option value="{{$item['id']}}" {{ ($item['id'] == old('id_karyawan')) ? 'selected="selected"' : '' }}>{{$item['nama']}}</option>
                    @endforeach
                @endif
              </select>
              @error('id_karyawan')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
              </div>
              <div class="mb-3">
                <label class="form-label">E-mail</label><span class="text-danger">*</span>
                <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}">
                @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
              <div class="mb-3">
                <label class="form-label">Password</label><span class="text-danger">*</span>
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}">
                @error('password')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
              <div class="d-flex justify-content-center">
                <a href="{{route('account.index')}}" class="btn btn-light me-2">
                    <span class="btn-icon-label">
                      <i data-feather="arrow-left" class="me-2"></i>
                        <span> Kembali </span>
                    </span>
                </a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </h5>
        </div>
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('page-script')
<script>
  @if($action=='edit')
    const account = {!! json_encode($account->toArray()) !!}
    email.value = account.email
  @endif

</script>
@endsection
