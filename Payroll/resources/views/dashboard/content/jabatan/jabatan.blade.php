@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">

  <h1 class="h3 mb-3">Jabatan</h1>

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pt-4">
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="card-title mb-0">Jabatan</h5>
            <a class="btn btn-primary btn-action ms-2" data-bs-toggle="modal" data-bs-target="#add-modal">
              Add Jabatan</a>
          </div>

        </div>
        <div class="card-body">
          <table id="MyTable" class="table table-bordered" style="width:100%">
            <thead class="table-primary">
              <tr class="text-center">
                <th>
                  No.
                </th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Action</th>

              </tr>
            </thead>
            <tbody>
              @forelse($jabatan as $item)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$item->kode}}</td>
                <td>{{$item->nama}}</td>
                <td width="10" class="text-center">
                  <div class="d-flex justify-content-center">
                  <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#edit-modal"
                    data-bs-act="{{ route('jabatan.update', $item->id) }}" data-bs-nama="{{$item->nama}}" data-bs-kode="{{$item->kode}}"
                    style="margin-right:6px;">
                    <i class="fas fa-edit"></i>
                  </button>
                  <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delete-modal"
                    data-bs-id="{{$item->id}}" data-bs-act="{{ route('jabatan.destroy', $item->id) }}"
                    data-bs-nama="{{$item->nama}}"><i class="far fa-trash-alt"></i>
                  </button>
                  </div>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="8">
                  <center>Data kosong</center>
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add  -->
<div class="modal fade" id="add-modal" role="dialog" aria-labelledby="addData" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{ route('jabatan.store') }}" method="POST" class="requires-validation" novalidate>
        <div class="modal-header">
          <h5 class="modal-title" id="DataLabel"><i class="far fa-plus-square"></i>&nbsp; Tambah Data Jabatan</h5>
        </div>
        <div class="modal-body">
          {{csrf_field()}}
          <div class="mb-3">
            <label class="form-label">Kode</label><span class="text-danger">*</span>
            <input type="text" class="form-control" name="kode" id="kode" required>
            <div class="invalid-feedback">Kode is Required!</div>
          </div>
          <div class="mb-3">
            <label class="form-label">Nama</label><span class="text-danger">*</span>
            <input type="text" class="form-control" name="nama" id="nama" required>
            <div class="invalid-feedback">Nama is Required!</div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Tambahkan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal edit -->
<div class="modal modal-confirmation fade" id="edit-modal" role="dialog" aria-labelledby="editData"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="edit-form" action="/" method="post" class="requires-validation" novalidate>
        <div class="modal-body">
          @csrf
          @method('PUT')
          <h4>Ubah Data Jabatan</h4>
          <div class="mb-3">
            <label class="form-label">Kode</label><span class="text-danger">*</span>
            <input type="text" class="form-control" name="kode" id="kode" required>
            <div class="invalid-feedback">Kode is Required!</div>
          </div>
          <div class="mb-3">
            <label class="form-label">Nama</label><span class="text-danger">*</span>
            <input type="text" class="form-control" name="nama" id="nama" required>
            <div class="invalid-feedback">Nama is Required!</div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-link" data-bs-dismiss="modal">
            Batal
          </button>
          <button type="submit" class="btn btn-primary">
            <b>Simpan</b>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal delete -->
<div class="modal modal-confirmation fade" id="delete-modal" tabindex='-1' role="dialog" aria-labelledby="deleteData"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form id="delete-form" action="/" method="post">
        <div class="modal-body">
          <h4>Konfirmasi</h4>
          <p class="mb-0">Yakin untuk menghapus <strong id="del-name"> </strong></p>
        </div>
        <div class="modal-footer">
          @csrf
          @method('DELETE')
          <button type="button" class="btn btn-link" data-bs-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-danger">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('page-script')
{{-- FORM VALIDATION --}}
<script>
  (function () {
    'use strict'
    const forms = document.querySelectorAll('.requires-validation')
    Array.from(forms).forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }
          form.classList.add('was-validated')
        }, false
      )
    })
  })()
</script>

<script>
  document.addEventListener("DOMContentLoaded", x => {

    const editModal = document.getElementById('edit-modal');
    const editForm = document.getElementById('edit-form');
    const deleteModal = document.getElementById('delete-modal');
    const deleteForm = document.getElementById('delete-form');

    //edit modal script
    editModal.addEventListener('show.bs.modal', evt=>{
      let editBtn = event.relatedTarget;
      editForm.action = editBtn.getAttribute('data-bs-act');
      editModal.querySelector('#kode').value = editBtn.getAttribute('data-bs-kode');
      editModal.querySelector('#nama').value = editBtn.getAttribute('data-bs-nama');
    })

    // Delete Modal Script
    deleteModal.addEventListener('show.bs.modal', evt => {
      let deleteBtn = event.relatedTarget;
      deleteForm.action = deleteBtn.getAttribute('data-bs-act');
      deleteModal.querySelector('#del-name').textContent = '"' + deleteBtn.getAttribute('data-bs-nama') + '" ?'
    })

  });
</script>
@endsection