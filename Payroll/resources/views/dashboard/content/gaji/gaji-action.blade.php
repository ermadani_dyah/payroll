@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">
  <div class="row d-flex justify-content-center">
    <div class="col-8">
        @if($action == 'add')
            <h1 class="h3 mb-3">Add New gaji</h1>
        @else
            <h1 class="h3 mb-3">Edit gaji</h1>
        @endif
      <div class="card">
        <div class="card-header">
          <h5 class="card-title mb-0">
            @if($action == 'add')
                <form id="gaji-action" action="{{ route('gaji.store') }}" method="POST" enctype="multipart/form-data">
            @else
                <form id="gaji-action" action="{{ route('gaji.update', $gaji->id) }}" method="post" enctype="multipart/form-data">
                @method('put')
            @endif
                @csrf
                <div class="mb-3">
                    <label for="fullname" class="form-label">Karyawan</label><span class="text-danger">*</span>
                    <select class="form-select @error('id_karyawan') is-invalid @enderror" name="id_karyawan" aria-label="Default select example" id="id_karyawan">
                      <option selected disabled value="">Karyawan Anda</option>
                        @foreach ($karyawan as $item)
                          <option value="{{$item['id']}}">{{$item['nama']}}</option>
                        @endforeach
                      @if ($action=="edit")
                        @foreach ($karyawan as $item)
                          <option value="{{$item['id']}}" <?php if($gaji->id_karyawan == $item['id']) { echo "selected";} ?>>{{$item['nama']}}</option>
                        @endforeach
                      @endif
                      @if(old('id_karyawan'))
                          @foreach($karyawan as $item)
                          <option value="{{$item['id']}}" {{ ($item['id'] == old('id_karyawan')) ? 'selected="selected"' : '' }}>{{$item['nama']}}</option>
                          @endforeach
                      @endif
                    </select>
                    @error('id_karyawan')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                  <label for="fullname" class="form-label">Tunjangan</label><span class="text-danger">*</span>
                  <input type="number" name="tunjangan" class="form-control @error('tunjangan') is-invalid @enderror" value="{{ old('tunjangan') }}" id="tunjangan" placeholder="Tunjangan anda">
                  @error('tunjangan')
                      <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
              </div>
              <div class="mb-3">
                <label for="fullname" class="form-label">Gaji Pokok</label><span class="text-danger">*</span>
                <input type="text" name="gaji_pokok" class="form-control @error('gaji_pokok') is-invalid @enderror" value="{{ old('gaji_pokok') }}" id="gaji_pokok" placeholder="Gaji Pokok anda">
                @error('gaji_pokok')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

              <div class="d-flex justify-content-center">
                <a href="{{route('gaji.index')}}" class="btn btn-light me-2">
                    <span class="btn-icon-label">
                      <i data-feather="arrow-left" class="me-2"></i>
                        <span> Kembali </span>
                    </span>
                </a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </h5>
        </div>
        <div class="card-body">
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('page-script')
<script>

@if($action=='edit')
  const gaji = {!! json_encode($gaji->toArray()) !!}

    tunjangan.value = gaji.tunjangan
    gaji_pokok.value = gaji.gaji_pokok
@endif

</script>
@endsection