@extends('dashboard.dashboard')

@section('content')
<div class="container-fluid p-0">

  <h1 class="h3 mb-3">Gaji</h1>

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header pt-4">
          <div class="d-flex justify-content-between align-items-center">
            <h5 class="card-title mb-0">Gaji</h5>
            @if (Auth::user()->id_role==1)
            <a href="{{route('gaji.create')}}" type="button" class="btn btn-primary">
              Add Gaji</a>
            @endif
          </div>

        </div>
        <div class="card-body">
          <table id="MyTable" class="table table-bordered" style="width:100%">
            <thead class="table-primary">
              <tr class="text-center">
                <th>
                  No.
                </th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Action</th>

              </tr>
            </thead>
            <tbody>
              @forelse($gaji as $item)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$item->karyawan->nama}}</td>
                <td>{{$item->karyawan->jabatan->nama}}</td>
                <td width="10" class="text-center">
                  <div class="dropstart d-flex justify-content-center">
                      <button class="btn" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="{{ route('gaji.show', $item->id) }}">Show</a></li>
                        @if (Auth::user()->id_role==1)
                          <li><a class="dropdown-item" href="{{ route('gaji.edit', $item->id) }}">Edit</a></li>
                          <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#delete-modal" data-bs-id="{{$item->id}}" data-bs-act="{{ route('gaji.destroy', $item->id) }}" data-bs-kode="{{$item->karyawan->nama}}">Delete</a></li>
                        @endif
                      </ul>
                  </div>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="8">
                  <center>Data kosong</center>
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-confirmation fade" id="delete-modal" tabindex='-1' role="dialog" aria-labelledby="deleteData" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
          <form id="delete-form" action="/" method="post" enctype="multipart/form-data">
              @csrf
              <div class="modal-body">
                  <h4>Konfirmasi</h4>
                  <p class="mb-0">Yakin untuk menghapus <strong id="del-name"> </strong></p>
              </div>
              <div class="modal-footer">
                  @method('DELETE')
                  <button type="button" class="btn btn-link" data-bs-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-danger">Hapus</button>
              </div>
          </form>
      </div>
  </div>
</div>

@endsection
<script>
  document.addEventListener("DOMContentLoaded", x => {

      const deleteModal = document.getElementById('delete-modal');
      const deleteForm = document.getElementById('delete-form');

      // Delete Modal Script
      deleteModal.addEventListener('show.bs.modal', evt => {
          let deleteBtn = event.relatedTarget;
          deleteForm.action = deleteBtn.getAttribute('data-bs-act');
          deleteModal.querySelector('#del-name').textContent = '"' + deleteBtn.getAttribute('data-bs-kode') + '" ?'
      })

  });
</script>