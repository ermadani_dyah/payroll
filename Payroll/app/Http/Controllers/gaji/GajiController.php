<?php

namespace App\Http\Controllers\gaji;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gaji\Gaji;
use App\Models\karyawan\Karyawan;
use App\Models\transaksi\Transaksi;
use App\Models\detail\Detail;
use App\Http\Requests\gaji\GajiStore;
use App\Http\Requests\gaji\GajiUpdate;
use Illuminate\Support\Facades\Auth;
class GajiController extends Controller
{
    Const FETCHED_ATTRIBUTE = [
        'id_karyawan',
        'tunjangan',
        'gaji_pokok'
    ];
    public function index()
    {
        $user=Auth::user();
        if($user->id_role==1){
            $gaji = Gaji::with(['karyawan.detail','karyawan.jabatan'])->orderBy('created_at', 'desc')
            ->get();
            return view('dashboard.content.gaji.gaji', compact('gaji'));
        }else{
            $gaji = Gaji::where('id_karyawan',$user->id_karyawan)->with(['karyawan.detail','karyawan.jabatan'])->orderBy('created_at', 'desc')
            ->get();
            return view('dashboard.content.gaji.gaji', compact('gaji'));
        }
       
    }

    public function create()
    {
        $karyawan = Karyawan::get();
        $action = 'add';
        return view('dashboard.content.gaji.gaji-action', compact('action','karyawan'));
    }

    public function store(GajiStore $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $gaji = Gaji::create($data);
        return redirect('/gaji')->with(['success' => 'Data Berhasil Ditambahkan!']);
        
    }
    public function show($id)
    {
         $gaji = Gaji::where('id', $id)->with(['karyawan.detail','karyawan.jabatan'])->first();
         $transaksi=Detail::where('id_karyawan',$gaji->id_karyawan)->get();
         return view('dashboard.content.gaji.gaji-show', compact('gaji','transaksi'));
    }

    public function edit($id)
    {
        $action = 'edit';
        $karyawan = Karyawan::get();
        $gaji = gaji::findOrFail($id)->load('karyawan.detail.transaksi','karyawan.jabatan');
        return view('dashboard.content.gaji.gaji-action', compact('action','gaji','karyawan'));
    }

    public function update(GajiUpdate $request, Gaji $gaji)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
            $gaji->update($data);
            return redirect('/gaji')->with(['success' => 'Data Berhasil Diupdate!']);
      
        
    }

    public function destroy(Gaji $gaji)
    {
        $gaji->delete();
        return redirect('/gaji')->with(['success' => 'Data Berhasil DiHapus!']);
    }
}
