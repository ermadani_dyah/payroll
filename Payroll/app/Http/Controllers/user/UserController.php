<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\UserStore;
use App\Http\Requests\User\UserUpdate;
use App\Models\User;
use App\Models\karyawan\Karyawan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    const FETCHED_ATTRIBUTE = [
        'id_role',
        'id_karyawan',
        'name',
        'email',
        'password',
        'isActive'
    ];
    public function index(Request $request)
    {
        $account = User::orderBy('created_at', 'desc')
            ->get();
        return view('dashboard.content.user.user', compact('account'));
    }

    public function create()
    {
        $karyawan = Karyawan::get();
        $action = 'add';
        return view('dashboard.content.user.user-action', compact('action','karyawan'));
    }

    public function store(UserStore $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['id_role'] = 1;
        $data['isActive'] = 1;
        $data['password']=Hash::make($data['password']);
        $karyawan = Karyawan::findOrFail($data['id_karyawan']);
        $data['name']=$karyawan->nama;
        $account = User::create($data);
        
        return redirect('account')->with('success', 'Account berhasil ditambah !');
    }

    public function edit($id)
    {
        $karyawan = Karyawan::get();
        $action = 'edit';
        $account = User::findOrFail($id);
        return view('dashboard.content.user.user-action', compact('action', 'account','karyawan'));
    }

    public function update(UserUpdate $request,User $account)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['password']=Hash::make($data['password']);
        $karyawan = Karyawan::findOrFail($data['id_karyawan']);
        $data['name']=$karyawan->nama;
        $account->update($data);

        return redirect('account')->with('success', 'Account berhasil diedit !');
    }

    public function destroy(User $user)
    {
        
        $user->delete();
        return redirect('account')->with('success', 'Account berhasil dihapus !');
    }


    public function activate($id, Request $request)
    {
        $user = User::findOrFail($id);
        $data = $request->all();
        $user->update([
            'isActive' => 1
        ]);


        return redirect('/account')->with('success', 'Account berhasil diaktifkan !');
    }

    public function ban($id, Request $request)
    {
        $user = User::findOrFail($id);
        $data = $request->all();
        $user->update([
            'isActive' => 0
        ]);

        return redirect('/account')->with('success', 'Account berhasil diban !');
    }
}
