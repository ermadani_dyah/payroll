<?php

namespace App\Http\Controllers\jabatan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\jabatan\Jabatan;
use App\Http\Requests\Jabatan\StoreJabatan;
use App\Http\Requests\Jabatan\UpdateJabatan;
class JabatanController extends Controller
{
    const FETCHED_ATTRIBUTE = [
        'nama',
        'kode'
    ];
    public function index()
    {
        $jabatan = Jabatan::orderBy('created_at', 'desc')
        ->get();

        return view('dashboard.content.jabatan.jabatan', compact('jabatan'));
    }
    public function create()
    {
        //
    }
    public function store(StoreJabatan $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);

        $jabatan = Jabatan::create($data);
        return redirect('/master/jabatan')->with(['success' => 'Jabatan Berhasil Ditambah!']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

  
    public function update(UpdateJabatan $request,Jabatan $jabatan)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $jabatan->update($data);
        return redirect('/master/jabatan')->with(['success' => 'Jabatan Berhasil Diubah!']);
    }

    public function destroy(Jabatan $jabatan)
    {
        $jabatan->delete();
        return redirect('/master/jabatan')->with(['success' => 'Jabatan Berhasil Dihapus!']);
    }
}
