<?php

namespace App\Http\Controllers\karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\karyawan\Karyawan;
use App\Models\jabatan\Jabatan;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\karyawan\KaryawanStore;
use App\Http\Requests\karyawan\KaryawanUpdate;
use Illuminate\Support\Facades\Http;
class karyawanController extends Controller
{
    Const FETCHED_ATTRIBUTE = [
        'id_jabatan',
        'email',
        'nama',
        'alamat',
        'kabupaten',
        'kecamatan',
        'provinsi',
        'rekening'
    ];
    public function index()
    {
        $provinsi = Http::get('https://region-api.profileimage.studio/provinces');
        $data_provinsi = $provinsi->json('data');

        $kabupaten = Http::get('https://region-api.profileimage.studio/regencies');
        $data_kabupaten = $kabupaten->json('data');

        $kecamatan = Http::get('https://region-api.profileimage.studio/districts');
        $data_kecamatan = $kecamatan->json('data');
        $karyawan = Karyawan::with(['jabatan'])->orderBy('created_at', 'desc')
        ->get();

        return view('dashboard.content.karyawan.karyawan', compact('karyawan','data_provinsi','data_kecamatan','data_kabupaten'));
    }
    public function create()
    {
        
        $jabatan = Jabatan::get();
        $kecamatan = Http::get('http://region-api.profileimage.studio/districts');
        $data_kecamatan = $kecamatan->json('data');
        $kabupaten = Http::get('https://region-api.profileimage.studio/regencies');
        $data_kabupaten = $kabupaten->json('data');
        $provinsi = Http::get('https://region-api.profileimage.studio/provinces');
        $data_provinsi = $provinsi->json('data');
        $action = 'add';
        return view('dashboard.content.karyawan.karyawan-action', compact('action','data_provinsi','data_kecamatan','data_kabupaten','jabatan'));
    }

    public function store(KaryawanStore $request)
    {
        // dd('ss');
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $karyawan = karyawan::create($data);
        User::create([
            'id_role' => 2,
            'id_karyawan'=>$karyawan->id,
            'password'=>Hash::make($data['rekening']),
            'name' => $data['nama'],
            'email' => $data['email'],
            'isActive' => 1,

        ]);
        return redirect('/master/karyawan')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $jabatan = Jabatan::get();
        $kecamatan = Http::get('http://region-api.profileimage.studio/districts');
        $data_kecamatan = $kecamatan->json('data');
        $kabupaten = Http::get('https://region-api.profileimage.studio/regencies');
        $data_kabupaten = $kabupaten->json('data');
        $provinsi = Http::get('https://region-api.profileimage.studio/provinces');
        $data_provinsi = $provinsi->json('data');
        $action = 'edit';
        $karyawan = karyawan::findOrFail($id);
        return view('dashboard.content.karyawan.karyawan-action', compact('action','data_provinsi','data_kecamatan','data_kabupaten','jabatan','karyawan'));
    }

    public function update(KaryawanUpdate $request, karyawan $karyawan)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $karyawan->update($data);
        User::where('id_karyawan',$karyawan->id)->update([
            'id_role' => 2,
            'id_karyawan'=>$karyawan->id,
            'password'=>Hash::make($data['rekening']),
            'name' => $data['nama'],
            'email' => $data['email'],
            'isActive' => 1,

        ]);
        return redirect('/master/karyawan')->with(['success' => 'Karyawan Berhasil Diubah!']);
    }

    public function destroy(karyawan $karyawan)
    {
        $user=User::where('id_karyawan',$karyawan->id)->delete();
        $karyawan->delete();
        return redirect('/master/karyawan')->with(['success' => 'Data Berhasil DiHapus!']);
    }
    public function kabupaten(Request $request)
    {
        $kabupaten = Http::get('https://region-api.profileimage.studio/regencies');
        $data_kabupaten = $kabupaten->json('data');
        $kab = [];
        foreach ($data_kabupaten as $item) {
            if ($item['province_id'] == $request->id) {
                array_push($kab, (object)[
                    'id' => $item['id'],
                    'name' => $item['name']
                ]);
            }
        }
        return response()->json($kab);
    }
    public function kecamatan(Request $request)
    {
        $kecamatan = Http::get('https://region-api.profileimage.studio/districts');
        $data_kecamatan = $kecamatan->json('data');
        $kec = [];
        foreach ($data_kecamatan as $item) {
            if ($item['regency_id'] == $request->id) {
                array_push($kec, (object)[
                    'id' => $item['id'],
                    'name' => $item['name']
                ]);
            }
        }
        return response()->json($kec);
    }
}
