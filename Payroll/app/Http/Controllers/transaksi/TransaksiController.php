<?php

namespace App\Http\Controllers\transaksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\transaksi\Transaksi;
use App\Models\karyawan\Karyawan;
use App\Models\detail\Detail;
use App\Models\gaji\Gaji;
use App\Http\Requests\transaksi\TransaksiStore;
use App\Http\Requests\transaksi\TransaksiUpdate;
class TransaksiController extends Controller
{
    Const FETCHED_ATTRIBUTE = [
        'id_karyawan',
        'id_transaksi',
        'nominal',
        'kode',
        'jumlah',
        'persen',
        'total'
    ];
    public function index()
    {
        $transaksi = Transaksi::with(['detail.karyawan'])->orderBy('created_at', 'desc')
        ->get();
        return view('dashboard.content.transaksi.transaksi', compact('transaksi'));
    }

    public function create()
    {
        $karyawan = Karyawan::get();
        $action = 'add';
        return view('dashboard.content.transaksi.transaksi-action', compact('action','karyawan'));
    }

    public function store(TransaksiStore $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $karyawan=$request->id_karyawan;
        $persen=$request->persen;
        $persenJumlah=array_sum($persen);

        if($persenJumlah!=100){
            return redirect('/transaksi')->with(['error' => 'Data Tidak 100%!']);
        }else{
            $transaksi = Transaksi::create($data);
            for ($i=0; $i < count($karyawan); $i++) {
                    $gaji= Gaji::where('id_karyawan',$karyawan[$i])->first();
                    $jumlah=$data['nominal'] * $persen[$i] / 100;

                    $total=null;
                    if($gaji){
                        $total=$gaji['tunjangan']+$gaji['gaji_pokok']+$jumlah;
                    }else{
                        $total=0;
                    }
                    
                    $save=array(
                        "id_karyawan"=>$karyawan[$i],
                        "id_transaksi"=>$transaksi->id,
                        "jumlah"=>$jumlah,
                        "persen"=>$persen[$i],
                        "total"=>$total
                    );
                    $detail=Detail::create($save);
            
                
            }
            return redirect('/transaksi')->with(['success' => 'Data Berhasil Ditambahkan!']);
        }
        
    }
    public function show($id)
    {
         $transaksi = Transaksi::where('id', $id)->with('detail.karyawan')->first();
         return view('dashboard.content.transaksi.transaksi-show', compact('transaksi'));
    }

    public function edit($id)
    {
        $action = 'edit';
        $karyawan = Karyawan::get();
        $transaksi = Transaksi::findOrFail($id)->load('detail.transaksi');
        return view('dashboard.content.transaksi.transaksi-action', compact('action','transaksi','karyawan'));
    }

    public function update(TransaksiUpdate $request, Transaksi $transaksi)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        
        $karyawan=$request->id_karyawan;
        $persen=$request->persen;
        $persenJumlah=array_sum($persen);

        if($persenJumlah!=100){
            return redirect('/transaksi')->with(['error' => 'Data Tidak 100%!']);
        }else{
            $transaksi->update($data);
            Detail::where('id_transaksi',$transaksi->id)->delete();
            for ($i=0; $i < count($karyawan); $i++) {
                    $jumlah=$data['nominal'] * $persen[$i] / 100;
                    $save=array(
                        "id_karyawan"=>$karyawan[$i],
                        "id_transaksi"=>$transaksi->id,
                        "jumlah"=>$jumlah,
                        "persen"=>$persen[$i],
                        "total"=>$jumlah
                    );
                    
                    $detail=Detail::create($save);

            }
            return redirect('/transaksi')->with(['success' => 'Data Berhasil Diupdate!']);
        }
        
    }

    public function destroy(Transaksi $transaksi)
    {
        $detail=Detail::where('id_transaksi',$transaksi)->delete();
        $transaksi->delete();
        return redirect('/transaksi')->with(['success' => 'Data Berhasil DiHapus!']);
    }
}
