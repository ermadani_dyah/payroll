<?php

namespace App\Http\Requests\Jabatan;

use Illuminate\Foundation\Http\FormRequest;

class UpdateJabatan extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'kode'=>'sometimes',
            'nama' => 'sometimes|string'
        ];
    }
}
