<?php

namespace App\Http\Requests\Jabatan;

use Illuminate\Foundation\Http\FormRequest;

class StoreJabatan extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'kode'=>'required|unique:jabatans',
            'nama' => 'required|string'
        ];
    }
}
