<?php

namespace App\Http\Requests\Karyawan;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\jabatan\Jabatan;
class KaryawanStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'id_jabatan' => 'required|numeric|exists:' . Jabatan::class . ',id',
            'email' => 'required|string|unique:users',
            'nama' => 'required|string',
            'rekening' => 'required|numeric',
            'alamat' => 'required|string',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required'
        ];
    }
}
