<?php

namespace App\Http\Requests\Karyawan;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\jabatan\Jabatan;
class KaryawanUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id_jabatan' => 'sometimes|numeric|exists:' . Jabatan::class . ',id',
            'email' => 'sometimes|string',
            'nama' => 'sometimes|string',
            'rekening' => 'sometimes|numeric',
            'alamat' => 'sometimes|string',
            'provinsi' => 'sometimes',
            'kabupaten' => 'sometimes',
            'kecamatan' => 'sometimes'
        ];
    }
}
