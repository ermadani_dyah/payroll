<?php

namespace App\Http\Requests\Gaji;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\karyawan\Karyawan;
class GajiStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id_karyawan' => 'required|numeric|exists:' . Karyawan::class . ',id',
            'gaji_pokok' => 'required|numeric',
            'tunjangan' => 'required|numeric',
        ];
    }
}
