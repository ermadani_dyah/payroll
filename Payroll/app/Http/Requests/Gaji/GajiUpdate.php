<?php

namespace App\Http\Requests\Gaji;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\karyawan\Karyawan;

class GajiUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'id_karyawan' => 'sometimes|numeric|exists:' . Karyawan::class . ',id',
            'gaji_pokok' => 'sometimes|numeric',
            'tunjangan' => 'sometimes|numeric',
        ];
    }
}
