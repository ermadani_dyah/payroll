<?php

namespace App\Models\gaji;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\karyawan\Karyawan;
use App\Models\detail\Detail;
class Gaji extends Model
{
    use HasFactory;
    protected $primaryKey='id';
    protected $table='gajis';
    protected $fillable=['id_karyawan','tunjangan','gaji_pokok'];
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class, "id_karyawan", "id");
    }
}
