<?php

namespace App\Models\transaksi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\detail\Detail;
class Transaksi extends Model
{
    use HasFactory;
    protected $primaryKey='id';
    protected $table='transaksis';
    protected $fillable=['kode','nominal'];
    public function detail()
    {
        return $this->hasMany(Detail::class, "id_transaksi", "id");
    }
}
