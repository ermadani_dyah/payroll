<?php

namespace App\Models\Karyawan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\jabatan\Jabatan;
use App\Models\gaji\Gaji;
use App\Models\detail\Detail;
class Karyawan extends Model
{
    use HasFactory;
    protected $primaryKey='id';
    protected $table='karyawans';
    protected $fillable=['nama','email','id_jabatan','alamat','kabupaten','kecamatan','provinsi','rekening'];
    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class, "id_jabatan", "id");
    }
    public function gaji()
    {
        return $this->hasMany(Gaji::class, "id_karyawan", "id");
    }
    public function detail()
    {
        return $this->hasMany(Detail::class, "id_karyawan", "id");
    }
}