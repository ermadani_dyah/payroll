<?php

namespace App\Models\jabatan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\karyawan\Karyawan;
class Jabatan extends Model
{
    use HasFactory;
    protected $primaryKey='id';
    protected $table='jabatans';
    protected $fillable=['nama','kode'];
    public function karyawan()
    {
        return $this->hasMany(Karyawan::class, "id", "id_jabatan");
    }
}
