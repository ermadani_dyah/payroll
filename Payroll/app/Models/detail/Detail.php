<?php

namespace App\Models\detail;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\karyawan\Karyawan;
use App\Models\transaksi\Transaksi;
use App\Models\gaji\Gaji;
class Detail extends Model
{
    use HasFactory;
    protected $primaryKey='id';
    protected $table='details';
    protected $fillable=['id_karyawan','id_transaksi','persen','jumlah','total'];
    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class, "id_karyawan", "id");
    }
    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, "id_transaksi", "id");
    }
}
